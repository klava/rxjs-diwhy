import { Observable } from "./Observable";

const o = new Observable(observer => observer.next("my value"));
o.subscribe(console.log);
