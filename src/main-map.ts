import { timer } from "./Observable";
import { map, filter } from "./operators";

timer(1000)
    .pipe(
        map(n => `tick ${n}`)
    )
    .subscribe(console.log);

timer(500)
    .pipe(
        filter(n => n > 2),
        map(n => `n after filter ${n}`)
    )
    .subscribe(console.log);
