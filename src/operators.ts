import { Observable } from "./Observable";

export const map = <T, R>(project: (T) => R) => (
  source: Observable<T>
) => {
  return new Observable(observer => {
    source.subscribe(val => observer.next(project(val)));
  });
};

export const filter = <T>(predicate: (T) => boolean) => (
  source: Observable<T>
) => {
  return new Observable(observer => {
    source.subscribe(val => {
      if (predicate(val)) {
        observer.next(val);
      }
    });
  });
};
