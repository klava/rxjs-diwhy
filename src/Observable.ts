interface Observer<T> {
    next(val: T): void;
}

interface Subscriber<T> {
    (observer: Observer<T>): void;
}

interface Operator<R, T> {
    (source: Observable<R>): Observable<T>;
}

export class Observable<T> {
    constructor(private subscriber: Subscriber<T>) {}

    subscribe(onNext: (val: T) => void) {
        this.subscriber({
            next: onNext
        });
    }

    pipe(): Observable<T>;
    pipe<A>(op1: Operator<T, A>): Observable<A>;
    pipe<A, B>(
        op1: Operator<T, A>,
        op2: Operator<A, B>
    ): Observable<B>;
    pipe<A, B, C>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>
    ): Observable<C>;
    pipe<A, B, C, D>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>,
        op4: Operator<C, D>
    ): Observable<D>;
    pipe<A, B, C, D, E>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>,
        op4: Operator<C, D>,
        op5: Operator<D, E>
    ): Observable<E>;
    pipe<A, B, C, D, E, F>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>,
        op4: Operator<C, D>,
        op5: Operator<D, E>,
        op6: Operator<E, F>
    ): Observable<F>;
    pipe<A, B, C, D, E, F, G>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>,
        op4: Operator<C, D>,
        op5: Operator<D, E>,
        op6: Operator<E, F>,
        op7: Operator<F, G>
    ): Observable<G>;
    pipe<A, B, C, D, E, F, G, H>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>,
        op4: Operator<C, D>,
        op5: Operator<D, E>,
        op6: Operator<E, F>,
        op7: Operator<F, G>,
        op8: Operator<G, H>
    ): Observable<H>;
    pipe<A, B, C, D, E, F, G, H, I>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>,
        op4: Operator<C, D>,
        op5: Operator<D, E>,
        op6: Operator<E, F>,
        op7: Operator<F, G>,
        op8: Operator<G, H>,
        op9: Operator<H, I>
    ): Observable<I>;
    pipe<A, B, C, D, E, F, G, H, I>(
        op1: Operator<T, A>,
        op2: Operator<A, B>,
        op3: Operator<B, C>,
        op4: Operator<C, D>,
        op5: Operator<D, E>,
        op6: Operator<E, F>,
        op7: Operator<F, G>,
        op8: Operator<G, H>,
        op9: Operator<H, I>,
        ...operations: Operator<any, any>[]
    ): Observable<{}>;

    pipe(...ops: Operator<any, any>[]) {
        return ops.reduce((prev, op) => op(prev), this);
    }
}

export const timer = (interval: number) =>
    new Observable(observer => {
        let n = 1;
        setInterval(() => {
            observer.next(n++);
        }, interval);
    });
