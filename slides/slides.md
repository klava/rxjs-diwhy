title: Observables (ReactiveX) - DiWHY
output: slides/index.html
theme: sjaakvandenberg/cleaver-dark
controls: false
style: style.css

--

# [DiWHY](http://reddit.com) Observables
## Do it yourself [RxJS](https://github.com/ReactiveX/rxjs) 

--

### Overview

1. Intro
2. Why [ReactiveX](http://reactivex.io)
3. Live coding
    * Observable class
    * interval constructor
    * pipe
    * filter and map operators
--

### Intro - [/r/DiWHY](http://reddit.com/r/diwhy)

<video controls preload>
    <source src="./iphone-case.webm" type="video/webm">
</video>

---

### Intro - [/r/DiWHY](http://reddit.com/r/diwhy)

<a href="https://imgur.com/a/5ztIaP9">
    <img src="./egg-chair/1.jpg" />
</a>

--


# [ReactiveX](http://reactivex.io)
## An API for asynchronous programming with observable streams

--

### [ReactiveX](http://reacitvex.io)

<div class="box">
    <ul>
        <li>Java: <a href="https://github.com/ReactiveX/RxJava">RxJava</a></li>
        <li>JavaScript: <a href="https://github.com/ReactiveX/rxjs">RxJS</a></li>
        <li>C#: <a href="https://github.com/Reactive-Extensions/Rx.NET">Rx.NET</a></li>
        <li>C#(Unity): <a href="https://github.com/neuecc/UniRx">UniRx</a></li>
        <li>Scala: <a href="https://github.com/ReactiveX/RxScala">RxScala</a></li>
        <li>Clojure: <a href="https://github.com/ReactiveX/RxClojure">RxClojure</a></li>
        <li>C++: <a href="https://github.com/Reactive-Extensions/RxCpp">RxCpp</a></li>
        <li>Lua: <a href="https://github.com/bjornbytes/RxLua">RxLua</a></li>
        <li>Ruby: <a href="https://github.com/Reactive-Extensions/Rx.rb">Rx.rb</a></li>
    </ul>

    <ul>
        <li>Python: <a href="https://github.com/ReactiveX/RxPY">RxPY</a></li>
        <li>Go: <a href="https://github.com/ReactiveX/RxGo">RxGo</a></li>
        <li>Groovy: <a href="https://github.com/ReactiveX/RxGroovy">RxGroovy</a></li>
        <li>JRuby: <a href="https://github.com/ReactiveX/RxJRuby">RxJRuby</a></li>
        <li>Kotlin: <a href="https://github.com/ReactiveX/RxKotlin">RxKotlin</a></li>
        <li>Swift: <a href="https://github.com/kzaher/RxSwift">RxSwift</a></li>
        <li>PHP: <a href="https://github.com/ReactiveX/RxPHP">RxPHP</a></li>
        <li>Elixir: <a href="https://github.com/alfert/reaxive">reaxive</a></li>
        <li>Dart: <a href="https://github.com/ReactiveX/rxdart">RxDart</a></li>
    </ul>
</div>

--

### Strengths

- Declarative
- Cancellation
- Debounce/throttle/buffer
- Retries
- Complex state over time

--

### Promises vs Observables

|          | Single     | Multiple     |
| ---:     | ---        | ---          |
| **Pull** | `Function` | `Iterator`   |
| **Push** | `Promise`  | `Observable` |

--

### Promise - single value

```typescript
const p = new Promise(
    resolve => resolve("my value")
)
p.then(value => console.log(value))
```

--

### Constructor - Observable

```typescript
const o = new Observable(
    observer => observer.next("my value")
)
o.subscribe(value => console.log(value))
```

--

### Constructor - interval

```typescript
const i: Observable<number> = interval(1000)
i.subscribe(console.log)
// 1 … 2 … 3 …
```

--

### Operators - map

```typescript
interval(1000)
    .pipe(
        map(value => `tick ${value}`)
    )
    .subscribe(console.log)
// "tick 1" … "tick 2" … "tick 3" …
```

--

### Operators - filter

```typescript
interval(1000)
    .pipe(
        filter(value => value > 2)
    )
    .subscribe(console.log)
// … … "tick 3" … "tick 4" …
```

--

### TODO

* 22 constructors (24 total)
* 102 operators (104 total)
* Observable completion
* Observable errors
* Hot/Cold observable

